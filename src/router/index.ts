import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/loginView.vue')
    },
    {
      path: '/verify',
      name: 'verify',
      component: () => import('../views/verifyView.vue')
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import('../views/signupView.vue')
    },
    {
      path: '/dashboard/overview',
      name: 'overview',
      component: () => import('../views/overView.vue')
    }
  ]
})

export default router
